package com.xyzio.analogdesert;

import java.util.ArrayList;
import java.util.List;

import com.xyzio.analogdesert.adapters.GenericAdapterSingleItem;
import com.xyzio.analogdesert.containers.GenericContainer;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class DropletMenu extends Activity {

	private ListView listView1;
	GenericAdapterSingleItem adapter;
	List<GenericContainer> data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_droplet_menu);

		data = new ArrayList<GenericContainer>();

		//Fill in data here
        //TODO: Reneable droplet items one by one
//		data.add(new GenericContainer("New", "new"));
//		data.add(new GenericContainer("Destroy", "destroy"));
		data.add(new GenericContainer("Details", "details"));
		data.add(new GenericContainer("Disable Backups", "disable_backups"));
		data.add(new GenericContainer("Enable Backups", "enable_backups"));
		data.add(new GenericContainer("Password Reset", "password_reset"));
		data.add(new GenericContainer("Power Cycle", "power_cycle"));
		data.add(new GenericContainer("Power Off", "power_off"));
		data.add(new GenericContainer("Power On", "power_on"));
		data.add(new GenericContainer("Reboot", "reboot"));
//		data.add(new GenericContainer("Rebuild", "rebuild"));
//		data.add(new GenericContainer("Rename", "rename"));
//		data.add(new GenericContainer("Resize", "resize")); //new
//		data.add(new GenericContainer("Restore", "restore")); //new
		data.add(new GenericContainer("Shutdown", "shutdown"));
//		data.add(new GenericContainer("Snapshot", "snapshot"));
//		data.add(new GenericContainer("Visit", "visit"));

		
		adapter = new GenericAdapterSingleItem(this, R.layout.single_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		listView1.setAdapter( adapter ); 

		//on click listener
		listView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				try {
					String item = data.get(position).item_name;
					String value = data.get(position).item_value;

					if (item.toLowerCase().contains("details") == true) {
                        Log.w("myApp","Calling Droplets.class");
						Intent intent = new Intent(DropletMenu.this, Droplets.class);
						startActivity(intent);
					}
					/*else if (item.toLowerCase().contains("rebuild") == true) {
						Intent intent = new Intent(DropletMenu.this, DropletActionRebuild.class);
						startActivity(intent);
					}
					else if (item.toLowerCase().contains("new") == true) {
						Intent intent = new Intent(DropletMenu.this, DropletActionNew.class);
						startActivity(intent);
					}
					else if (item.toLowerCase().contains("resize") == true) {
						Intent intent = new Intent(DropletMenu.this, DropletActionResize.class);
						startActivity(intent);
					}
					else if (item.toLowerCase().contains("restore") == true) {
						Intent intent = new Intent(DropletMenu.this, DropletActionRestore.class);
						startActivity(intent);
					}*/
					else {
						Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();

						Intent intent = new Intent(DropletMenu.this, DropletAction.class);
						intent.putExtra("title", item);
						intent.putExtra("action", value);

						startActivity(intent);
					}
				}
				catch (Exception ex) {
					Log.w("myApp", ex.toString());
				}
			}
		});
	}

}
