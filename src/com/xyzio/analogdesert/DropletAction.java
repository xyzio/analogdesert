package com.xyzio.analogdesert;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.xyzio.analogdesert.adapters.DropletAdapter;
import com.xyzio.analogdesert.libs.Action;
import com.xyzio.analogdesert.libs.AnalogDesertInterface;
import com.xyzio.analogdesert.libs.Droplet;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class DropletAction extends FragmentActivity implements GetSingleTextInputDialogFragment.NoticeDialogListener {

	private ListView listView1;
	DropletAdapter adapter;
	List<Droplet> data; 
	String dropletID;
	String action = "";
	String metadata = "";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_droplet_action);
			
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			//Stuff to get with adapter
			action  = extras.getString("action", "");
			Log.w("myApp", "Getting action: " + action);

			setTitle(extras.getString("title", action));
		}

		//TODO: Handle case where action is null

		data = new ArrayList<Droplet>();

		adapter = new DropletAdapter(this, 
				R.layout.droplets_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		listView1.setAdapter(adapter);

		//on click listener
		listView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				try {
					String droplet_name = data.get(position).name;
					Toast.makeText(getBaseContext(), droplet_name + " " + getTitle(), Toast.LENGTH_LONG).show();

					dropletID = data.get(position).id;

					if (action.toLowerCase().contains("rename")) {

						DialogFragment newFragment = new GetSingleTextInputDialogFragment();
						Bundle args = new Bundle();
						args.putString("sTitle", "Enter New Name");
						newFragment.setArguments(args);

						newFragment.show(getSupportFragmentManager(), "rename");
					}
					else if (action.toLowerCase().contains("snapshot")) {

						DialogFragment newFragment = new GetSingleTextInputDialogFragment();
						Bundle args = new Bundle();
						args.putString("sTitle", "Enter Snapshot Name");
						newFragment.setArguments(args);

						newFragment.show(getSupportFragmentManager(), "snapshot");
					}
					/*else if (action.toLowerCase().contains("ping")) {
						Runtime runtime = Runtime.getRuntime();
						Process proc = runtime.exec("ping fr.yahoo.com -c 1"); // other servers, for example
						proc.waitFor();
						int exit = proc.exitValue();
						if (exit == 0) { // normal exit
	
						} else { // abnormal exit, so decide that the server is not reachable
						 
						}
					}*/
                    //TODO: Re-enable visit page
					/*else if (action.toLowerCase().contains("visit")) {
						Uri uriUrl = Uri.parse("http://" + data.get(position).ip_address);  
						Log.w("myApp", "Visit: URI is " + uriUrl.toString());
						Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl); 
						startActivity(launchBrowser);  
					}*/
					else {
						new PerformDropletAction().execute(dropletID);
					}
				}
				catch (Exception ex) {
					Log.w("myApps", ex.toString());
				}
			}
		});

		try {
			new DownloadDroplets().execute();
		}
		catch (Exception ex) {
			Log.w("MyApp", ex.toString());
		}

		onResume();		
	}

	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String result) {
		// User touched the dialog's positive button
		Toast.makeText(getBaseContext(), "OK " + result, Toast.LENGTH_LONG).show();
		metadata = result;
		dialog.dismiss();

		new PerformDropletAction().execute(dropletID);
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// User touched the dialog's negative button
		Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_LONG).show();

		finish();
	}
	

	class PerformDropletAction extends AsyncTask<String, Void, com.xyzio.analogdesert.libs.Action> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletAction.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletAction.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage(getTitle() + " " + " ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Action doInBackground(String... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletAction.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Action dropletAction;

		/*	if (action.toLowerCase().contains("rename") == true) {
				droplets = AnalogDesertInterface.renameDroplet(auth_key, arg0[0], metadata);
			}
			else if (action.toLowerCase().contains("snapshot") == true) {
				droplets = AnalogDesertInterface.snapshotDroplet(auth_key, arg0[0], metadata);
			}
			else */
				dropletAction = AnalogDesertInterface.getDropletResponse(auth_key, arg0[0], action);

			return (dropletAction);
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Action action) {
			super.onPostExecute(action);

			dialog.dismiss();

			errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});

            //TODO: Enable error logging.  Maybe with exception catch
			/*if (droplets.status == null) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("Null response recieved. Unable to complete " + getTitle());
			}
			else if (droplets.status.toLowerCase().contains("error") == true) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(droplets.error_message);
			}
			else if (droplets.event_id != null) {
				errorDialog.setTitle("Success!");
				errorDialog.setMessage(getTitle() + " complete. Event ID " + droplets.event_id);
			}
			else {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("Invalid response " + getTitle());				
			} */
            errorDialog.setTitle("Sent");
            errorDialog.setMessage(getTitle() + " sent");
			errorDialog.show();
		}
	} 

	//---------------------
	//http://developer.android.com/reference/android/os/AsyncTask.html
	class DownloadDroplets extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Droplets> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletAction.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletAction.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Downloading Droplets ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Droplets doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletAction.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Droplets droplets = new com.xyzio.analogdesert.libs.Droplets();

			try {
				droplets = AnalogDesertInterface.getDroplets(auth_key);

			} catch (Exception e) {
				exceptionMessage = e.toString();
			}

			//TODO: Read response = ERROR too

			return (droplets); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Droplets weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();

            //TODO: Capture error message. Maybe display exceptionMessage here
			/* if ((weather_data == null) || (weather_data.status.toLowerCase().contains("error") == true)) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(weather_data.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else*/ if (weather_data.droplets.size() > 0)
				DropletAction.this.data.addAll(weather_data.droplets);

			adapter.notifyDataSetChanged();
		}
	}
}
