package com.xyzio.analogdesert;

import com.xyzio.analogdesert.domains.DomainMenu;
import com.xyzio.analogdesert.libs.AnalogDesertInterface;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
//import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

		setContentView(R.layout.activity_main);
		onResume();
	}

	@Override
	public void onResume() {
		super.onResume();

		new CheckStatus().execute();
	}

	public void viewDropletMenu(View view) {
		try {
			startActivity(new Intent(this, DropletMenu.class));
		}
		catch (Exception ex) {
			Log.w("myApp", ex.toString());
		}
	}

    //TODO: Enable these one by one
	/*public void viewSizes(View view) {
		try {
			startActivity(new Intent(this, SizeMenu.class));
		}
		catch (Exception ex) {
			Log.w("myApp", ex.toString());
		}
	}
	
	public void viewDomains(View view) {
		try {
			startActivity(new Intent(this, DomainMenu.class));
		}
		catch (Exception ex) {
			Log.w("myApp", ex.toString());
		}
	}
	
	public void viewImages(View view) {
		try {
			startActivity(new Intent(this, ImageMenu.class));
		}
		catch (Exception ex) {
			Log.w("myApp", ex.toString());
		}
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);

		switch(item.getItemId()) {
		case R.id.menu_settings:
			startActivity(new Intent(this, PreferencesActivity.class));
			return (true);
		}	
		return (false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}


	class CheckStatus extends AsyncTask<Void, Void, String> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(MainActivity.this).create();
		//private final ProgressDialog dialog = new ProgressDialog(MainActivity.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();
			//dialog.setMessage("Checking connectivity ...");
			//dialog.show();
		}

		protected String doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

			String auth_key = sharedPref.getString("auth_key", "");

			return(AnalogDesertInterface.verifyKeys(auth_key));
		}

		protected void onPostExecute(String status) {
			super.onPostExecute(status);	

			//dialog.dismiss();

			final TextView mTextView = (TextView) findViewById(R.id.textview_title);

			if (status.toLowerCase().contains("false") == true) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("Unable to connect to DigitalOcean. Please check your keys in the settings menu and verify your connectivity ...");
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {

					}
				});
				mTextView.setText("Unable to connect to DigitalOcean. Please check your keys in the settings menu and verify your connectivity ...");
				mTextView.setTextColor(Color.RED);
				errorDialog.show();
			}
			else {
				mTextView.setText("");
			}
		}
	} 

}
