package com.xyzio.analogdesert.containers;

public class GenericContainer {
	public String item_name = "";
	public String item_value = "";
	public String item_id = "";
	
	public GenericContainer() {}
	
	public GenericContainer(String name, String value) {
		this.item_name = name;
		this.item_value = value;
	}
	
	public GenericContainer(String name, String value, String item_id) {
		this.item_name = name;
		this.item_value = value;
		this.item_id = item_id;
	}
}
