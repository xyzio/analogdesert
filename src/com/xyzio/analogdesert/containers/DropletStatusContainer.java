package com.xyzio.analogdesert.containers;

public class DropletStatusContainer {
	public String item_name = "";
	public String item_value = "";
	
	public DropletStatusContainer() {}
	public DropletStatusContainer(String name, String value) {
		
		this.item_name = name;
		this.item_value = value;
	}
	
	
}