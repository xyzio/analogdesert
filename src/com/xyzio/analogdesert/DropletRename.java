package com.xyzio.analogdesert;

import com.xyzio.analogdesert.GetSingleTextInputDialogFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


public class DropletRename extends FragmentActivity implements GetSingleTextInputDialogFragment.NoticeDialogListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_droplet_rename);
		
		
		DialogFragment newFragment = new GetSingleTextInputDialogFragment();
		Bundle args = new Bundle();
		args.putString("sTitle", "Rename");
		newFragment.setArguments(args);
		
		//Show keyboard
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		
		newFragment.show(getSupportFragmentManager(), "missiles");
	}

/*	public void showNoticeDialog() {
		// Create an instance of the dialog fragment and show it
		DialogFragment dialog = new FireMissilesDialogFragment();
		dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
	}*/

	// The dialog fragment receives a reference to this Activity through the
	// Fragment.onAttach() callback, which it uses to call the following methods
	// defined by the NoticeDialogFragment.NoticeDialogListener interface
	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String result) {
		// User touched the dialog's positive button
		Toast.makeText(getBaseContext(), "OK " + result, Toast.LENGTH_LONG).show();
		dialog.dismiss();
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// User touched the dialog's negative button
		Toast.makeText(getBaseContext(), "Cancel", Toast.LENGTH_LONG).show();
	}
}


