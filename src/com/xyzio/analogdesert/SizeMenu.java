package com.xyzio.analogdesert;

import java.util.ArrayList;
import java.util.List;


import com.xyzio.analogdesert.adapters.GenericAdapter;
import com.xyzio.analogdesert.containers.GenericContainer;
import com.xyzio.analogdesert.libs.AnalogDesertInterface;
import com.xyzio.analogdesert.libs.Sizes;


import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;

public class SizeMenu extends Activity {

	private ListView listView1;
	GenericAdapter adapter;
	List<GenericContainer> data;  

	String action = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sizes);

		data = new ArrayList<GenericContainer>();

		adapter = new GenericAdapter(this, R.layout.droplets_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		new GetSizes().execute();

		listView1.setAdapter( adapter ); 
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	//	TODO: Add options menu to all screens
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_images, menu);
		return true;
	}


	class GetSizes extends AsyncTask<Void, Void, Sizes> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(SizeMenu.this).create();
		private final ProgressDialog dialog = new ProgressDialog(SizeMenu.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf

		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();
			dialog.setMessage("Downloading Sizes ...");
			dialog.show();           
		}

		protected Sizes doInBackground(Void... arg0) {

			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(SizeMenu.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Sizes sizes = new Sizes();
			try {
				sizes = AnalogDesertInterface.getSizes(auth_key);
			} catch (Exception e) {
				Log.w("myApp", "Exception at SizeMenu.java" + e.toString());
				exceptionMessage = e.toString();
			}
			return (sizes); 
		}

		protected void onPostExecute(Sizes sizes) {
			super.onPostExecute(sizes);	


			dialog.dismiss();

			errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});


			//Populate adapter
			 if (sizes.sizes.size() > 0) {
				for (int i = 0; i < sizes.sizes.size(); i++) {
					SizeMenu.this.data.add(new GenericContainer("id:\t" + sizes.sizes.get(i).slug + "", "size:\t\t" + sizes.sizes.get(i).memory));
				}
			}
			else {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("No Images Found");
				errorDialog.show();
				return;
			}
			
			adapter.notifyDataSetChanged();
		}
	} 
}
