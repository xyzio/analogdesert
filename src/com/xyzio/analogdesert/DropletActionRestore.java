package com.xyzio.analogdesert;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.xyzio.analogdesert.adapters.DropletAdapter;
import com.xyzio.analogdesert.libs.Action;
import com.xyzio.analogdesert.libs.AnalogDesertInterface;
import com.xyzio.analogdesert.libs.Droplet;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class DropletActionRestore extends FragmentActivity implements ListInputDialogFragment.NoticeDialogListener {

	private ListView listView1;
	DropletAdapter adapter;
	List<Droplet> data;
	BigInteger[] dropletIDs;
	String action = "";
	String metadata = "";

	ArrayList<String> imageIdToIndexMap = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_droplet_action);

		data = new ArrayList<Droplet>();

		adapter = new DropletAdapter(this, 
				R.layout.droplets_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		listView1.setAdapter(adapter);

		//on click listener
		listView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				try {
					String droplet_name = data.get(position).name;
					Toast.makeText(getBaseContext(), droplet_name + " " + getTitle(), Toast.LENGTH_LONG).show();
					dropletIDs = new BigInteger[] {	new BigInteger(data.get(position).id) };

					//Get images
					Toast.makeText(getBaseContext(), "Getting images", Toast.LENGTH_LONG).show();
					new DownloadImages().execute();

				}
				catch (Exception ex) {
					Log.w("myApps", ex.toString()); //TODO: Change this to dialog box and return to droplet menu
				}
			}
		});

		try {
			new DownloadDroplets().execute();
		}
		catch (Exception ex) {
			Log.w("MyApp", ex.toString());
		}

		onResume();		
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String result) {
		// User touched the dialog's positive button
		Integer nResult = Integer.parseInt(result);
		metadata = imageIdToIndexMap.get(nResult);
		dialog.dismiss();
		new PerformDropletAction().execute(dropletIDs);
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// User touched the dialog's negative button
		Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_LONG).show();

		finish();
	}


	class PerformDropletAction extends AsyncTask<BigInteger, Void, com.xyzio.analogdesert.libs.Action> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletActionRestore.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletActionRestore.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage(getTitle() + " " + " ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Action doInBackground(BigInteger... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletActionRestore.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			Action action = AnalogDesertInterface.restoreDroplet(auth_key, arg0[0], metadata); //image_id is stored in metadata

			return (action);
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Action action) {
			super.onPostExecute(action);

			dialog.dismiss();

			errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});

            //TODO: Fix error handling
            /*
			if (droplets.status == null) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("Null response recieved. Unable to complete " + getTitle());
			}
			else if (droplets.status.toLowerCase().contains("error") == true) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(droplets.error_message);
			}
			else if (droplets.event_id != null) {
				errorDialog.setTitle("Success!");
				errorDialog.setMessage(getTitle() + " complete. Event ID " + droplets.event_id);
			}
			else {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("Invalid response " + getTitle());				
			}
*/
			errorDialog.show();
		}
	} 

	//---------------------
	class DownloadImages extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Images> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletActionRestore.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletActionRestore.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Downloading Images ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Images doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletActionRestore.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Images images = new com.xyzio.analogdesert.libs.Images();

			try {
				images = AnalogDesertInterface.getImages(auth_key, "my_images");
			} catch (Exception e) {
				exceptionMessage = e.toString();
			}

			//TODO: Read response = ERROR too

			return (images); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Images weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();
			ArrayList<String> imageList = new ArrayList<String>();

            //TODO: Fix error handling
            /*
			if ((weather_data == null) || (weather_data.status.toLowerCase().contains("error") == true)) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(weather_data.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else */
            if (weather_data.images.size() > 0) {

				//Toast.makeText(getBaseContext(), "Found " + weather_data.images.size() + " images", Toast.LENGTH_LONG).show();
				for (int i = 0; i < weather_data.images.size(); i += 1) {
					imageList.add(weather_data.images.get(i).name);
					imageIdToIndexMap.add(weather_data.images.get(i).id + "");		
					//Toast.makeText(getBaseContext(), "Adding Image" + weather_data.images.get(i).name, Toast.LENGTH_LONG).show();
				}

				//Rebuild. Need to get image ID for base
				DialogFragment newFragment = new ListInputDialogFragment();
				Bundle args = new Bundle();
				args.putString("sTitle", "Select Image");
				args.putStringArrayList("list", imageList);

				newFragment.setArguments(args);

				newFragment.show(getSupportFragmentManager(), "rebuild");
				
			}
			else {
				Toast.makeText(getBaseContext(), "Image count is 0", Toast.LENGTH_LONG).show();
			}
			
			//adapter.notifyDataSetChanged();
		}
	}


	//---------------------
	//---------------------
	//http://developer.android.com/reference/android/os/AsyncTask.html
	class DownloadDroplets extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Droplets> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletActionRestore.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletActionRestore.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Downloading Droplets ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Droplets doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletActionRestore.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Droplets droplets = new com.xyzio.analogdesert.libs.Droplets();

			try {
				droplets = AnalogDesertInterface.getDroplets(auth_key);
			} catch (Exception e) {
				exceptionMessage = e.toString();
			}

			//TODO: Read response = ERROR too
			return (droplets); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Droplets weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();

            //TODO: Fix error handling
            /*
			if ((weather_data == null) || (weather_data.status.toLowerCase().contains("error") == true)) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(weather_data.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else*/ if (weather_data.droplets.size() > 0)
				DropletActionRestore.this.data.addAll(weather_data.droplets);

			adapter.notifyDataSetChanged();
		}
	}
}
