package com.xyzio.analogdesert;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.xyzio.analogdesert.adapters.DropletAdapter;
//import com.xyzio.analogdesert.containers.GenericContainer;
import com.xyzio.analogdesert.libs.AnalogDesertInterface;
import com.xyzio.analogdesert.libs.Droplet;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;


public class Droplets extends Activity {

	private ListView listView1;
	DropletAdapter adapter;
	List<Droplet> data;  

	@Override
	public void onCreate(Bundle savedInstanceState) {
        Log.w("myApp", "Droplets.java reached");
		super.onCreate(savedInstanceState);

		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

		setContentView(R.layout.activity_droplets);

		//Set ListView title
		//        TextView titleTxtView = (TextView)findViewById(R.id.txtHeader);
		//        titleTxtView.setText("Droplets");

		data = new ArrayList<Droplet>();

		adapter = new DropletAdapter(this, 
				R.layout.droplets_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		listView1.setAdapter(adapter);


		//on click listener
		listView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				try {


					String item = data.get(position).name;
					Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();

//Create intent
Intent intent = new Intent(Droplets.this, DropletStatus.class);
//Create JSON object from datastructure
Gson gson = new Gson();
String json = gson.toJson(data.get(position));
//Add JSON to intent
intent.putExtra("json", json);
//Start intent
startActivity(intent);
				}
				catch (Exception ex) {
					Log.w("myapps", ex.toString());
				}
			}
		});




		try {

			new DownloadDroplets().execute();

		}
		catch (Exception ex) {
			Log.w("MyApp", ex.toString());
		}

		onResume();		
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	//http://developer.android.com/reference/android/os/AsyncTask.html
	class DownloadDroplets extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Droplets> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(Droplets.this).create();
		private final ProgressDialog dialog = new ProgressDialog(Droplets.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();
			
			dialog.setMessage("Downloading Droplets ...");
			dialog.show();
        
		}

		protected com.xyzio.analogdesert.libs.Droplets doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(Droplets.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Droplets droplets = new com.xyzio.analogdesert.libs.Droplets();
			//GenericContainer container = new GenericContainer();
			
			try {
				droplets = AnalogDesertInterface.getDroplets(auth_key);
			} catch (Exception e) {
				exceptionMessage = e.toString();
			}
			
			//TODO: Read response = ERROR too

			return (droplets); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Droplets weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();

			if (weather_data == null) {
				errorDialog.setTitle("Error");
				//errorDialog.setMessage(weather_data.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else if (weather_data.droplets.size() > 0)
				Droplets.this.data.addAll(weather_data.droplets);

			adapter.notifyDataSetChanged();
		}
	} 
}
