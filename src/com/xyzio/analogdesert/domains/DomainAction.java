package com.xyzio.analogdesert.domains;

import java.util.ArrayList;
import java.util.List;


import com.xyzio.analogdesert.R;
import com.xyzio.analogdesert.adapters.GenericAdapterSingleItem;
import com.xyzio.analogdesert.containers.GenericContainer;
import com.xyzio.analogdesert.domains.records.DomainRecordMenu;
import com.xyzio.analogdesert.libs.AnalogDesertInterface;
import com.xyzio.analogdesert.libs.Domains;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class DomainAction extends FragmentActivity implements com.xyzio.analogdesert.ListInputDialogFragment.NoticeDialogListener {

	private ListView listView1;
	GenericAdapterSingleItem adapter;
	List<GenericContainer> data;  

	Domains globalDomains = null;
	String DomainID;

	String action = "";
	String title = "";

	String regionID = "";
	ArrayList<String> regionIdToIndexMap = new ArrayList<String>();
	ArrayList<String> regionList = new ArrayList<String>();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.generic_listview);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			//Stuff to get with adapter
			action  = extras.getString("action", "");
			title = extras.getString("title", "");
			setTitle(title);
		}

		data = new ArrayList<GenericContainer>();

		adapter = new GenericAdapterSingleItem(this, R.layout.single_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		//new GetDomains().execute();

		listView1.setAdapter( adapter ); 

        /*
		listView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				try {
					//Toast.makeText(getBaseContext(), data.get(position).item_name + " " + getTitle(), Toast.LENGTH_LONG).show();
					//DomainID = data.get(position).item_id;


					String item = data.get(position).item_name;
					String domainId = data.get(position).item_id;

					if (action.toLowerCase().contains("view") == true) {
						Intent intent = new Intent(DomainAction.this, DomainDisplay.class);
						intent.putExtra("id", globalDomains.domains.get(position).id);
						intent.putExtra("name", globalDomains.domains.get(position).name);
						intent.putExtra("ttl", globalDomains.domains.get(position).ttl);
						intent.putExtra("live_zone_file", globalDomains.domains.get(position).live_zone_file);
						intent.putExtra("error", globalDomains.domains.get(position).error);
						intent.putExtra("zone_file_with_error", globalDomains.domains.get(position).zone_file_with_error);
						startActivity(intent);
					}
					if (action.toLowerCase().contains("records") == true) {
						Intent intent = new Intent(DomainAction.this, DomainRecordMenu.class);
						intent.putExtra("id", globalDomains.domains.get(position).id);
						intent.putExtra("name", globalDomains.domains.get(position).name);
						intent.putExtra("ttl", globalDomains.domains.get(position).ttl);
						intent.putExtra("live_zone_file", globalDomains.domains.get(position).live_zone_file);
						intent.putExtra("error", globalDomains.domains.get(position).error);
						intent.putExtra("zone_file_with_error", globalDomains.domains.get(position).zone_file_with_error);
						startActivity(intent);
					}
					if (action.toLowerCase().contains("destroy") == true) {
						ArrayList<String> args = new ArrayList<String>();
						args.add(domainId);
						new PerformAction().execute(domainId);
					}
				}
				catch (Exception ex) {
					Log.w("myApps", ex.toString());
				}
			}
		}); */

	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String result) {
		// User touched the dialog's positive button
		//Integer nResult = Integer.parseInt(result);

		//if (currentAction.toLowerCase().contains("get_region")) {
		//Only looking at region for now.  	
		//regionID = regionIdToIndexMap.get(nResult);
		//dialog.dismiss();

		//new PerformAction().execute((BigInteger[]) imageIDs);
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// User touched the dialog's negative button
		//Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_LONG).show();

		//finish();
	}

    /*
	class PerformAction extends AsyncTask<String, Void, Domains> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DomainAction.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DomainAction.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage(getTitle() + " " + " ...");
			dialog.show();
		}

		protected Domains doInBackground(String... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DomainAction.this);

			String api_key = sharedPref.getString("pref_api_key", "");
			String client_id = sharedPref.getString("pref_client_id", "");

			Domains domains = new Domains();

			try {
				if (action.toLowerCase().contains("destroy") == true) {
					domains = AnalogDesertInterface.destroyDomain(client_id, api_key, arg0[0]);
				}
				else if (action.toLowerCase().contains("destroy") == true) {
					domains = AnalogDesertInterface.destroyDomain(client_id, api_key);
				}
			}
			catch (Exception ex) {
				exceptionMessage = ex.getMessage();
			}

			return (domains); 
		}

		protected void onPostExecute(Domains domains) {
			super.onPostExecute(domains);	

			dialog.dismiss();

			errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});

			if (domains.status == null) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(exceptionMessage + ". Unable to complete " + getTitle());
			}
			else if (domains.status.toLowerCase().contains("error") == true) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(domains.error_message.toString());
			}
			else if (domains.status.toLowerCase().contains("ok") == true) {
				errorDialog.setTitle("Success!");
				errorDialog.setMessage(getTitle() + " complete.");
			}
			else {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("Invalid response " + getTitle());				
			}

			errorDialog.show();
		}
	} */

	/*
	class GetDomains extends AsyncTask<Void, Void, Domains> {


		private final AlertDialog errorDialog = new AlertDialog.Builder(DomainAction.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DomainAction.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf

		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();
			dialog.setMessage("Downloading Domains ...");
			dialog.show();           
		}

		protected Domains doInBackground(Void... arg0) {

			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DomainAction.this);

			String api_key = sharedPref.getString("pref_api_key", "");
			String client_id = sharedPref.getString("pref_client_id", "");

			Domains domains = new Domains();

			try {
				domains = AnalogDesertInterface.getDomains(client_id, api_key);

			} catch (Exception e) {
				//			Log.w("myApp", "Exception at Images.java" + e.toString());
				exceptionMessage = e.toString();
			}
			return (domains); 
		}

		protected void onPostExecute(Domains domains) {
			super.onPostExecute(domains);	

			dialog.dismiss();

			errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});

			if (domains.status == null || domains.status.toLowerCase().contains("error") == true) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("Error " + exceptionMessage);
				errorDialog.show();
				return;
			}
			//Populate adapter
			else if (domains.domains.size() > 0) {
				globalDomains = domains;
				for (int i = 0; i < domains.domains.size(); i++) {
					DomainAction.this.data.add(new GenericContainer(domains.domains.get(i).name, "", domains.domains.get(i).id));
				}
			}
			else {
				errorDialog.setTitle("Error");
				errorDialog.setMessage("No Domains Found");
				errorDialog.show();
				return;
			}

			adapter.notifyDataSetChanged();
		}
	} */
	//---------------------
	/*class DownloadRegions extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Regions> {

	private final AlertDialog errorDialog = new AlertDialog.Builder(ImageAction.this).create();
	private final ProgressDialog dialog = new ProgressDialog(ImageAction.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


	String exceptionMessage = "";

	protected void onPreExecute() {       
		super.onPreExecute();

		dialog.setMessage("Downloading Regions ...");
		dialog.show();
	}

	protected com.xyzio.analogdesert.libs.Regions doInBackground(Void... arg0) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(ImageAction.this);

		String api_key = sharedPref.getString("pref_api_key", "");
		String client_id = sharedPref.getString("pref_client_id", "");

		com.xyzio.analogdesert.libs.Regions regions = new com.xyzio.analogdesert.libs.Regions();
		try {
			regions = AnalogDesertInterface.getRegions(client_id, api_key);
		}
		catch (Exception ex){
			exceptionMessage = ex.getMessage();
		}
		return (regions); 
	}

	protected void onPostExecute(com.xyzio.analogdesert.libs.Regions weather_data) {
		super.onPostExecute(weather_data);	

		dialog.dismiss();


		if ((weather_data == null) || (weather_data.status.toLowerCase().contains("error") == true)) {
			errorDialog.setTitle("Error");
			errorDialog.setMessage(exceptionMessage + " " + weather_data.error_message);
			errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});
			errorDialog.show();
		}
		else if (weather_data.regions.size() > 0) {
			regionList.clear();
			regionIdToIndexMap.clear();
			regionID = "";

			for (int i = 0; i < weather_data.regions.size(); i += 1) {
				regionList.add(weather_data.regions.get(i).name);
				regionIdToIndexMap.add(weather_data.regions.get(i).id + "");		
			}

			//Rebuild. Need to get image ID for base
			DialogFragment newFragment = new ListInputDialogFragment();
			Bundle args = new Bundle();
			args.putString("sTitle", "Select Region");
			args.putStringArrayList("list", regionList);

			newFragment.setArguments(args);

			newFragment.show(getSupportFragmentManager(), "getRegion");
		}
	}
}*/
	//---------------------

}
