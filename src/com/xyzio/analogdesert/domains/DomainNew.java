package com.xyzio.analogdesert.domains;

import java.util.ArrayList;
import com.xyzio.analogdesert.ListInputDialogFragment;
import com.xyzio.analogdesert.R;
import com.xyzio.analogdesert.libs.AnalogDesertInterface;
import com.xyzio.analogdesert.libs.Domains;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


//TODO: Convert this to listview in the future!!

public class DomainNew extends FragmentActivity implements ListInputDialogFragment.NoticeDialogListener {

	ArrayList<String> DropletIpList = new ArrayList<String>();
	ArrayList<String> DropletNameList = new ArrayList<String>();
	//List<Size> sizes = new ArrayList<Size>();

	String[] IpAddress;
	String action = "";
	String metadata = "";

	String domainID = "";
	String IpAddressID = "";


	ArrayList<String> IpAddressIdToIndexMap = new ArrayList<String>();

	ArrayList<String> IpList = new ArrayList<String>();

	String currentAction = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_domain_new);

		onResume();		
	}

	@Override
	public void onResume() {
		super.onResume();
	}


	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.buttonSelectDoIp:
			currentAction = "get_ip";
			//new DownloadIPs().execute();
			break;
		case R.id.buttonCreateDomain:
			currentAction = "create";
			//Create();
			break;
		}
	}

	/*public void Create() {

		EditText domainEditText = (EditText) findViewById(R.id.textDomainEntry);
		domainID = domainEditText.getText().toString();

		if (domainID == null || domainID.isEmpty()) {
			Toast.makeText(getBaseContext(), "Error: Domain Name is null or empty", Toast.LENGTH_LONG).show();
			return;
		}
		
		EditText IpAddress =	(EditText)findViewById(R.id.editText1);
		IpAddressID = IpAddress.getText().toString();
		
		if (IpAddressID == null || IpAddressID.isEmpty()) {
			Toast.makeText(getBaseContext(), "Error: IP Address is null or empty", Toast.LENGTH_LONG).show();
			return;
		}

		new CreateDomain().execute();

		//This is where we create
	} */

	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String result) {
		// User touched the dialog's positive button
		Integer nResult = Integer.parseInt(result);
		if (currentAction.toLowerCase().contains(("get_ip"))) {
			IpAddressID = DropletIpList.get(nResult);

			EditText text =	(EditText)findViewById(R.id.editText1);
			text.setText(DropletIpList.get(nResult));
			dialog.dismiss();
		}
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// User touched the dialog's negative button
		//Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_LONG).show();
		//finish();
	}

        /*
	//---------------------
	//---------------------
	class CreateDomain extends AsyncTask<Void, Void, Domains> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DomainNew.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DomainNew.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Creating Domain ...");
			dialog.show();
		}

		protected Domains doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DomainNew.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			Domains domains = new Domains();

			try {
				//Log.w("myApp", "domainID = " + domainID + ", IP Address = " + IpAddressID);
				domains = AnalogDesertInterface.createDomain(auth_key, domainID, IpAddressID);

			} catch (Exception e) {
				exceptionMessage = e.toString();
			}

			return (domains); 
		}

		protected void onPostExecute(Domains domain) {
			super.onPostExecute(domain);	

			dialog.dismiss();

			if ((domain == null) || (domain.domain == null) || (domain.status.toLowerCase().contains("error") == true)) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(exceptionMessage + " " + domain.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						//finish();
					}
				});
				errorDialog.show();
			}
			else if ((domain.domain) != null && (domain.status.toLowerCase().contains("ok"))) {
				errorDialog.setTitle("Success");

				String message = "Domain Created\n";
				message += "Domain Name: " + domain.domains.get(0).name + "\n";
				message += "Domain ID: " + domain.domains.get(0).domain + "\n";
				errorDialog.setMessage(message);

				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				
				errorDialog.show();
			}
		}
	}
	//---------------------
	//http://developer.android.com/reference/android/os/AsyncTask.html
	class DownloadIPs extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Droplets> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DomainNew.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DomainNew.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf

		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Downloading Droplet IPs ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Droplets doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DomainNew.this);

			String api_key = sharedPref.getString("pref_api_key", "");
			String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Droplets droplets = new com.xyzio.analogdesert.libs.Droplets();

			try {
				droplets = AnalogDesertInterface.getDroplets(client_id, api_key);
			} catch (Exception e) {
				exceptionMessage = e.toString();
			}

			//TODO: Read response = ERROR too

			return (droplets); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Droplets droplets) {
			super.onPostExecute(droplets);	

			dialog.dismiss();

			if ((droplets == null) || (droplets.status.toLowerCase().contains("error") == true) || (droplets.droplets.size() == 0)) {
				errorDialog.setTitle("Error");
				String errorMessage = "";
				if (droplets.droplets.size() == 0) {
					errorMessage = "No droplets found. ";
				}
				errorMessage += exceptionMessage + " " + droplets.error_message;
				errorDialog.setMessage(errorMessage);
				
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else if (droplets.droplets.size() > 0) {
				DropletIpList.clear();
				DropletNameList.clear();
				
				for(int i = 0; i < droplets.droplets.size(); i += 1) {
					DropletIpList.add(droplets.droplets.get(i).ip_address);
					DropletNameList.add(droplets.droplets.get(i).name);
				}
				//launch pop-up for user to select IP address
				//Rebuild. Need to get image ID for base
				DialogFragment newFragment = new ListInputDialogFragment();
				Bundle args = new Bundle();
				args.putString("sTitle", "Select Droplet");
				args.putStringArrayList("list", DropletNameList);

				newFragment.setArguments(args);

				newFragment.show(getSupportFragmentManager(), "get_ip");
			}
		}
	}*/
}

