package com.xyzio.analogdesert.domains.records;

import com.xyzio.analogdesert.R;
import com.xyzio.analogdesert.R.layout;
import com.xyzio.analogdesert.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class DomainRecordDisplay extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_domain_record_display);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.domain_record_display, menu);
		return true;
	}

}
