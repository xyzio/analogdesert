package com.xyzio.analogdesert.domains;

import java.util.ArrayList;
import java.util.List;

import com.xyzio.analogdesert.R;
import com.xyzio.analogdesert.adapters.GenericAdapterSingleItem;
import com.xyzio.analogdesert.containers.GenericContainer;
import com.xyzio.analogdesert.domains.records.DomainRecordMenu;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class DomainMenu extends Activity {

	private ListView listView1;
	GenericAdapterSingleItem adapter;
	List<GenericContainer> data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_domain_menu);

		data = new ArrayList<GenericContainer>();

		//Fill in data here
		data.add(new GenericContainer("View", "view"));
		data.add(new GenericContainer("New", "newdomain"));
		data.add(new GenericContainer("Destroy", "destroy"));
		data.add(new GenericContainer("Records", "records"));
		
		/*data.add(new GenericContainer("View Details", "viewdetails"));
		data.add(new GenericContainer("New", "new"));
		data.add(new GenericContainer("Destroy", "destroy"));
		data.add(new GenericContainer("Records", "records"));
		*/
		
		adapter = new GenericAdapterSingleItem(this, R.layout.single_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		listView1.setAdapter(adapter); 

		//on click listener
		listView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				try {
					//String item = data.get(position).item_name;
					String value = data.get(position).item_value;

					if (value.toLowerCase().contains("view") == true) {
						Intent intent = new Intent(DomainMenu.this, DomainAction.class);
						intent.putExtra("action", value);
						intent.putExtra("title", "View Domains");
						startActivity(intent);
					}
					else if (value.toLowerCase().contains("newdomain") == true) {
						Intent intent = new Intent(DomainMenu.this, DomainNew.class);
						startActivity(intent);
					}
					else if (value.toLowerCase().contains("destroy") == true) {
						Intent intent = new Intent(DomainMenu.this, DomainAction.class);
						intent.putExtra("action", "destroy");
						intent.putExtra("title", "Destroy Domain");
						startActivity(intent);
					}
					else if (value.toLowerCase().contains("records") == true) {
						Intent intent = new Intent(DomainMenu.this, DomainAction.class);
						intent.putExtra("action", value);
						intent.putExtra("title", "Select Domain");
						startActivity(intent);
					}
				}
				catch (Exception ex) {
					Log.w("myApp", ex.toString());
				}
			}
		});
	}

}
