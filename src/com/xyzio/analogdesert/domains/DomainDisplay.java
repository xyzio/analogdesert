package com.xyzio.analogdesert.domains;

import java.util.ArrayList;
import java.util.List;


import com.xyzio.analogdesert.R;
import com.xyzio.analogdesert.adapters.GenericAdapter;
import com.xyzio.analogdesert.containers.GenericContainer;
import com.xyzio.analogdesert.libs.Domains;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class DomainDisplay extends FragmentActivity {

	Domains domains = null;
	
	private ListView listView1;
	GenericAdapter adapter;
	List<GenericContainer> data;  

	String DomainID;
	
	String id, name, ttl, live_zone_file, error, zone_file_with_error;

	String regionID = "";
	ArrayList<String> regionIdToIndexMap = new ArrayList<String>();
	ArrayList<String> regionList = new ArrayList<String>();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.generic_listview);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			//Stuff to get with adapter
			id = extras.getString("id", "");
			name = extras.getString("name", "");
			ttl = extras.getString("ttl", "");
			live_zone_file = extras.getString("live_zone_file", "");
			error = extras.getString("error", "");
			zone_file_with_error = extras.getString("zone_file_with_error", "");
		}

		data = new ArrayList<GenericContainer>();
		data.add(new GenericContainer("id", id));
		data.add(new GenericContainer("name", name));
		data.add(new GenericContainer("ttl", ttl));
		data.add(new GenericContainer("live_zone_file", "Touch To Expand : " + live_zone_file));

		if (error.isEmpty() == false)
			data.add(new GenericContainer("error", error));
		if (zone_file_with_error.isEmpty() == false)
			data.add(new GenericContainer("zone_file_with_error", zone_file_with_error));
		
		adapter = new GenericAdapter(this, R.layout.droplets_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		listView1.setAdapter( adapter ); 
		
		listView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				try {
					//Toast.makeText(getBaseContext(), data.get(position).item_name + " " + getTitle(), Toast.LENGTH_LONG).show();
					//DomainID = data.get(position).item_id;
					String item_name = data.get(position).item_name;
					if (item_name.toLowerCase().contains("live_zone_file")) {
						
						AlertDialog.Builder builder = new AlertDialog.Builder(DomainDisplay.this);
						
						builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
					           public void onClick(DialogInterface dialog, int id) {
					               // User clicked OK button
					           }
					       });
						
						builder.setMessage(live_zone_file)
					       .setTitle("live zone file");
						
						AlertDialog dialog = builder.create();
						dialog.show();
					}
				}

				catch (Exception ex) {
					Log.w("myApps", ex.toString());
				}
			}
		});

	}

	@Override
	public void onResume() {
		super.onResume();
	}

}
