package com.xyzio.analogdesert.adapters;


import java.util.List;

import com.xyzio.analogdesert.R;
import com.xyzio.analogdesert.containers.GenericContainer;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class GenericAdapter extends ArrayAdapter<GenericContainer>{

    Context context; 
    int layoutResourceId;    
    List<GenericContainer> data = null;
    
    public GenericAdapter(Context context, int layoutResourceId, List<GenericContainer> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Holder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new Holder();
            //holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.name = (TextView)row.findViewById(R.id.txtTitle);
            holder.value = (TextView)row.findViewById(R.id.txtSummary);
            
            row.setTag(holder);
        }
        else
        {
            holder = (Holder)row.getTag();
        }

        holder.name.setText(data.get(position).item_name);
        holder.name.setBackgroundColor(Color.GREEN);
        
        holder.value.setText(data.get(position).item_value);

        return row;
    }
    
    static class Holder
    {
        //ImageView imgIcon;
        TextView name;
        TextView value;
    }
}