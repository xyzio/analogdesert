package com.xyzio.analogdesert.adapters;

import java.util.List;

import com.xyzio.analogdesert.R;
import com.xyzio.analogdesert.libs.Droplet;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DropletAdapter extends ArrayAdapter<Droplet>{

	Context context; 
	int layoutResourceId;    
	List<Droplet> data = null;

	public DropletAdapter(Context context, int layoutResourceId, List<Droplet> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		DropletHolder holder = null;

		if(row == null)
		{
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new DropletHolder();
			holder.dropletName = (TextView)row.findViewById(R.id.txtTitle);
			holder.dropletInfo = (TextView)row.findViewById(R.id.txtSummary);

			row.setTag(holder);
		}
		else
		{
			holder = (DropletHolder)row.getTag();
		}

		String droplet = data.get(position).name;
		holder.dropletName.setText(droplet);

		//TODO: Fix coloring based on active or not active
        String ip = "IP not found";
        if (data.get(position).networks.v4 != null &&  data.get(position).networks.v4.size() > 0)
            ip = data.get(position).networks.v4.get(0).ip_address;
        else if (data.get(position).networks.v6 != null &&  data.get(position).networks.v6.size() > 0)
            ip = data.get(position).networks.v6.get(0).ip_address;

		holder.dropletInfo.setText(ip);

		if (data.get(position).status != null && data.get(position).status.toLowerCase().contains("active"))
			holder.dropletName.setBackgroundColor(Color.GREEN);
		else 
			holder.dropletName.setBackgroundColor(Color.RED);
		return row;
	}

	static class DropletHolder
	{
		TextView dropletName;
		TextView dropletInfo;
	}
}