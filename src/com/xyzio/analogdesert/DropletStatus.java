package com.xyzio.analogdesert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import com.google.gson.Gson;
import com.xyzio.analogdesert.adapters.DropletStatusAdapter;
import com.xyzio.analogdesert.containers.DropletStatusContainer;
import com.xyzio.analogdesert.libs.*;

import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;

public class DropletStatus extends Activity {


	private ListView listView1;
	DropletStatusAdapter adapter;
	List<DropletStatusContainer> data;  
	
	int id  = -1;

	int image_id = -1;
	int size_id = -1;
	int region_id = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        setTitle("Droplet Details");

		super.onCreate(savedInstanceState);
		
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		setContentView(R.layout.activity_droplet_status);

		data = new ArrayList<DropletStatusContainer>();
		
		adapter = new DropletStatusAdapter(this, R.layout.droplets_item_row, data);

		listView1 = (ListView)findViewById(R.id.listView1);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			//Stuff to get with adapter
            String json = extras.getString("json","-1");
            Gson gson = new Gson();

            Droplet droplet = gson.fromJson(json, Droplet.class);


				
			data.add(new DropletStatusContainer("name", droplet.name));

            for (Map.Entry<String, String> entry: droplet.getKeyValueMap().entrySet())
                data.add(new DropletStatusContainer(entry.getKey(), entry.getValue()));
		}
		
		//new GetDropletDetails().execute();
		
		listView1.setAdapter( adapter ); 
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_droplet_status, menu);
		return true;
	}

	class GetDropletDetails extends AsyncTask<Void, Void, List<DropletStatusContainer>> {

		private final ProgressDialog dialog = new ProgressDialog(DropletStatus.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf
		//private final AlertDialog errorDialog = new AlertDialog.Builder(DropletStatus.this).create();
		
		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();
			dialog.setMessage("Downloading Droplet Details ...");
			dialog.show();           
		}

		protected List<DropletStatusContainer> doInBackground(Void... arg0) {
			
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletStatus.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");
			
			List<DropletStatusContainer> values = new ArrayList<DropletStatusContainer>();

			try {
				//Get image name
				Images images = AnalogDesertInterface.getImageByImageId(auth_key, image_id);

				/*if (images.status.toLowerCase().contains("error"))
					values.add(new DropletStatusContainer("Image", images.error_message));
				else*/
					values.add(new DropletStatusContainer("Image", images.images.get(0).name));
					

				//Get size
				//Sizes sizes = AnalogDesertInterface.getSizes(auth_key);
				/*if (sizes.status.toLowerCase().contains("error"))
					values.add(new DropletStatusContainer("Size", sizes.error_message));

				else */
                //TODO: Fix get sizes
                /*
					for (int i = 0; i < sizes.sizes.size(); i++) {
						if (sizes.sizes.get(i).id == size_id) {
							values.add(new DropletStatusContainer("Size", sizes.sizes.get(i).name));
							break;
						}
					}
				*/
				//Get region ID
                //TODO: Fix get region
				//Regions regions = AnalogDesertInterface.getRegions(client_id, api_key);
				/*if (regions.status.toLowerCase().contains("error"))
					values.add(new DropletStatusContainer("Region", sizes.error_message));
				else*/
					/*for (int i = 0; i < regions.regions.size(); i++) {
						if (regions.regions.get(i).id == region_id) {
							values.add(new DropletStatusContainer("Region", regions.regions.get(i).name));
							break;
						}
					}*/
				
			} catch (Exception e) {
				Log.w("myApp", "Exception at DropletStatus.java" + e.toString());
				exceptionMessage = e.toString();
			}

			return (values); 
		}

		protected void onPostExecute(List<DropletStatusContainer> container) {
			super.onPostExecute(container);	

			dialog.dismiss();

			/*	if (image_name == "") {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(exceptionMessage + "\nPlease check your API Key and Client ID");
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else*/
			
			//Insert
			for (int i = 0; i < container.size(); i++) {
				DropletStatus.this.adapter.add(container.get(i));
			}
			
			//adapter = new DropletAdapter(this, 
			// R.layout.droplets_item_row, items);


			adapter.notifyDataSetChanged();
			//listView1 = (ListView)findViewById(R.id.listView1);

			//View header = (View)getLayoutInflater().inflate(R.layout.droplets_header_row, null);
			//listView1.addHeaderView(header);

			//listView1.setAdapter(adapter);
		}
	} 
}
