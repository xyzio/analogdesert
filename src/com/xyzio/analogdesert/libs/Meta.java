package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mkapoor on 9/16/2014.
 */
public class Meta {

    @SerializedName("total")
    public String total;

}
