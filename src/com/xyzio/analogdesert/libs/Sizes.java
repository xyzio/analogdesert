package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkapoor on 9/20/2014.
 */
public class Sizes {

    @SerializedName("sizes")
    public List<Size> sizes;

    @SerializedName("meta")
    public Meta meta;
}
