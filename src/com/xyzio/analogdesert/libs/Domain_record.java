package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mkapoor on 9/17/2014.
 */
public class Domain_record {
    /*
      "Domain_record": {
    "id": 10,
    "type": "CNAME",
    "name": "example",
    "data": "@",
    "priority": null,
    "port": null,
    "weight": null
  }
     */
    @SerializedName("id")
    public String id;

    @SerializedName("type")
    public String type;

    @SerializedName("name")
    public String name;

    @SerializedName("data")
    public String data;

    @SerializedName("priority")
    public String priority;

    @SerializedName("port")
    public String port;

    @SerializedName("weight")
    public String weight;

}
