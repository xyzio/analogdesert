package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkapoor on 9/17/2014.
 */
public class Images {

    @SerializedName("images")
    public List<Image> images;

    @SerializedName("meta")
    public Meta meta;
}
