package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mkapoor on 9/20/2014.
 */
public class Ssh_key {
    /*"id": 1,
            "fingerprint": "2a:68:5c:e3:56:7e:a9:88:75:39:f4:f2:9d:2a:71:db",
            "public_key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAQQDPeJTWZvNZiBxd9PBBS6W18wH6qjtP4FOu84ngCBszJlIF6NZoRPjtdOYz4gvgoS+HkfrlgjdVNupwxJT5uDwN example",
            "name": "Example Key" */

    @SerializedName("id")
    public String id;

    @SerializedName("fingerprint")
    public String fingerprint;

    @SerializedName("public_key")
    public String public_key;

    @SerializedName("name")
    public String name;

}
