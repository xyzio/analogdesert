package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkapoor on 9/15/2014.
 */

/* {
        "id": 3,
        "status": "completed",
        "type": "rename",
        "started_at": "2014-06-10T22:25:14Z",
        "completed_at": "2014-06-10T23:25:14Z",
        "resource_id": 123,
        "resource_type": "droplet",
        "Region": "nyc2"
        } */


public class Actions {

    @SerializedName("actions")
    public List<Action> actions;

    @SerializedName("Meta")
    public Meta meta;

}
