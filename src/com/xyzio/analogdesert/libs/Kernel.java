package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mkapoor on 9/20/2014.
 */
public class Kernel {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("version")
    public String version;

   /* "kernel": {
        "id": 485432986,
                "name": "DO-recovery-static-fsck",
                "version": "3.8.0-25-generic"
    }, */

    public Map<String, String> getKeyValueMap() {

        Map<String, String> keyValueMap = new HashMap<String, String>();

        if (id != null) keyValueMap.put("kernel id", id);
        if (name != null) keyValueMap.put("kernel name", name);
        if (version != null) keyValueMap.put("kernel version", version);

        return keyValueMap;
    }

}

