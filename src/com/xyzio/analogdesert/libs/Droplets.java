package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkapoor on 9/17/2014.
 */
public class Droplets {

    @SerializedName("droplets")
    public List<Droplet> droplets;

    @SerializedName("meta")
    public Meta meta;
}
