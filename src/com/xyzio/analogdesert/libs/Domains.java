package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkapoor on 9/17/2014.
 */
public class Domains {

    @SerializedName("Domains")
    public List<Domain> domains;

    @SerializedName("Meta")
    public Meta meta;

}

