package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkapoor on 9/20/2014.
 */
public class Regions {

    @SerializedName("regions")
    public List<Region> regions;

    @SerializedName("meta")
    public Meta meta;
}
