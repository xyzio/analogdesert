package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mkapoor on 9/17/2014.
 */
public class Domain {
    /*
      "domain": {
    "name": "example.com",
    "ttl": 1800,
    "zone_file": "Example zone file text..."
  }
     */

    @SerializedName("domain")
    public String domain;

    @SerializedName("name")
    public String name;

    @SerializedName("ttl")
    public String ttl;

    @SerializedName("zone_file")
    public String zone_file;
}
