package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mkapoor on 9/21/2014.
 */
public class Networks {

    @SerializedName("v4")
    public List<V4> v4;

    @SerializedName("v6")
    public List<V6> v6;


    public Map<String, String> getKeyValueMap() {

        Map<String, String> keyValueMap = new HashMap<String, String>();
        for (V4 v4temp: v4)
            keyValueMap.putAll(v4temp.getKeyValueMap());

        for (V6 v6temp: v6)
            keyValueMap.putAll(v6temp.getKeyValueMap());

        return keyValueMap;
    }

}
