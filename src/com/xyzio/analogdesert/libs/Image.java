package com.xyzio.analogdesert.libs;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mkapoor on 9/17/2014.
 */
public class Image {

    /*
    {
  "image": {
    "id": 449676392,
    "name": "Ubuntu 13.04",
    "distribution": null,
    "slug": null,
    "public": false,
    "regions": [
      "Region--1"
    ],
    "created_at": "2014-09-05T02:02:08Z"
  }
}
     */
@SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("distribution")
    public String distribution;

    @SerializedName("slug")
    public String slug;

    @SerializedName("public")
    public String pub;

    //TODO: Enable regions
    @SerializedName("regions")
    public List<String> regions;

    @SerializedName("created_at")
    public String created_at;


    public Map<String, String> getKeyValueMap() {

        Map<String, String> keyValueMap = new HashMap<String, String>();

        keyValueMap.put("image id", id);
        keyValueMap.put("image name", name);
        keyValueMap.put("image distribution",distribution);
        keyValueMap.put("image slug", slug);
        keyValueMap.put("image public", pub);
        keyValueMap.put("image regions", TextUtils.join(", ", regions));
        keyValueMap.put("image created_at", created_at);

        return keyValueMap;
    }


}
