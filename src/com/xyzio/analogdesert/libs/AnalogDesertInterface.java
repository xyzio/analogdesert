package com.xyzio.analogdesert.libs;

import android.util.Log;

import com.google.gson.Gson;
//import com.google.gson.annotations.SerializedName;
//import com.sun.net.ssl.internal.www.protocol.https.Handler;

import java.io.*;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class AnalogDesertInterface {

	//http://www.javacodegeeks.com/2011/01/android-json-parsing-gson-tutorial.html
	static int statusCode;

	//filter Optional, String, either "my_images" or "global"
	public static Images getImages(String auth_key, String filter) {

		//String url = "https://api.digitalocean.com/images/?client_id=" + client_id + "&api_key=" + api_key;
        String url = "https://api.digitalocean.com/v2/images/";

		if (filter != null) {
			url += "&filter=" + filter;
		}

		Log.w("myApp", "Get images url: " + url);
		InputStream stream = retrieveStream(url, auth_key);

		Gson gson = new Gson();

		Reader reader = new InputStreamReader(stream);

		Images images;
        images = gson.fromJson(reader, Images.class);

        return(images);
	}

	public static Domains createDomain(String auth_key, String domain, String ip_address) {
		//Log.w("myApp", "Domain is " + domain + " IP address is " + ip_address);

		//String base_url = "https://api.digitalocean.com/domains?";
        String base_url = "https://api.digitalocean.com/v2/domains?";

		HashMap<String, String> args = new HashMap<String, String>();
		//args.put("client_id", client_id);
		//args.put("api_key", api_key);
		args.put("name", domain);
		args.put("ip_address", ip_address);

		return(getDomainResponse(auth_key, args, base_url));
	}	

	public static Domains destroyDomain(String auth_key, String domain_id) {
	//https://api.digitalocean.com/domains/[domain_id]/destroy?client_id=[your_client_id]&api_key=[your_api_key]

		String base_url = "https://api.digitalocean.com/v2/domains/" + domain_id + "/destroy?";
		
		HashMap<String, String> args = new HashMap<String, String>();
		//args.put("client_id", client_id);
		//args.put("api_key", api_key);

		return(getDomainResponse(auth_key, args, base_url));
	
	}
	
	public static Domains getDomains(String auth_key) {

		//https://api.digitalocean.com/domains?client_id=[your_client_id]&api_key=[your_api_key]
		String base_url = "https://api.digitalocean.com/v2/domains?";

		
		HashMap<String, String> args = new HashMap<String, String>();
		//args.put("client_id", client_id);
		//args.put("api_key", api_key);
		
		return(getDomainResponse(auth_key, args, base_url));
	}

	static Domains getDomainResponse(String auth_key, HashMap<String, String> args, String base_url) {

		String url = base_url; //"https://api.digitalocean.com/domains/?";

		Boolean thisIsFirstEntry = true;
		for (Map.Entry<String, String> entry : args.entrySet())
		{
			
			if (!thisIsFirstEntry) {
				url += "&";
			}            
			url += entry.getKey() + "=" + entry.getValue();

			thisIsFirstEntry = false;
			
		}
		
		Log.w("myApp", "Domains URL: " + url);
		InputStream stream = retrieveStream(url, auth_key);

		Gson gson = new Gson();

		Reader reader = new InputStreamReader(stream);

		Domains domains = gson.fromJson(reader, Domains.class);

		Log.w("myApp", "Domains status count " + domains.meta.total);

		return(domains);
	}



	public static Regions getRegions(String auth_key) {


		//String url = "https://api.digitalocean.com/regions/?client_id=" + client_id + "&api_key=" + api_key;
        String url = "https://api.digitalocean.com/v2/regions/";

		Log.w("myApp", "Regions URL: " + url);
		InputStream stream = retrieveStream(url, auth_key);

		Gson gson = new Gson();

		Reader reader = new InputStreamReader(stream);

		Regions regions = gson.fromJson(reader, Regions.class);

		return(regions);

	}

    public static Action createDroplet(String auth_key, String name, String size_id, String image_id, String region_id) {

        //https://api.digitalocean.com/droplets/new?client_id=[your_client_id]&api_key=[your_api_key]&name=[droplet_name]&size_id=[size_id]&image_id=[image_id]&region_id=[region_id]

        String extra = "name=" + name + "&size_id=" + size_id + "&image_id=" + image_id + "&region_id=" + region_id;

        return getDropletResponse(auth_key, null, "new", extra);

    }



	public static Images getImageByImageId(String auth_key, int imageId) throws Exception {

		String url = "https://api.digitalocean.com/v2/images/" + imageId; //+ "/?client_id=" + client_id + "&api_key=" + api_key;


		Log.w("myApp", "Image URL = " + url);

		InputStream stream = retrieveStream(url, auth_key);

		Log.w("myApp", "StatusCode = " + statusCode);
		Log.w("myApp", "getImageByImageID response text: " + stream.toString());


		Gson gson = new Gson();

		Reader reader = new InputStreamReader(stream);

		Images response = gson.fromJson(reader, Images.class);

		Log.w("myApp", "Image response count " + response.meta.total);

		return (response);
	}

	public static Droplets getDropletById(String auth_key, int droplet_id) throws Exception {

		String url = "https://api.digitalocean.com/v2/droplets/" + droplet_id; // + "?client_id=" + client_id + "&api_key=" + api_key;

		Log.w("myApp", "Droplet getDropletById URL = " + url);

		InputStream stream = retrieveStream(url, auth_key);

		Log.w("myApp", "StatusCode = " + statusCode);

		Gson gson = new Gson();

		Reader reader = new InputStreamReader(stream);

		Droplets response = gson.fromJson(reader, Droplets.class);

		Log.w("myApp", "DropletStatus response count " + response.meta.total);

		return (response);


	}

	public static String verifyKeys(String auth_key) {

		//Lets get regions
		Regions regions;
		try {
			regions = getRegions(auth_key);
			Log.w("myApp", "Calling getRegions. Num regions found " + regions.regions.size());
		}
		catch (Exception ex) {
			return("false");
		}

		if (statusCode != 200) {
			return("false");
		}
		if (regions.regions.size() < 0) {
			return("false");
		}
		else { 
			return("true");
		}
	}

    public static Action restoreDroplet(String auth_key, BigInteger DropletID, String image_id) {
        // https://api.digitalocean.com/droplets/[droplet_id]/restore/?image_id=[image_id]&client_id=[your_client_id]&api_key=[your_api_key]
        return getDropletResponse(auth_key, DropletID.toString(), "restore", "image_id=" + image_id);
    }

    public static Action resizeDroplet(String auth_key, BigInteger DropletID, String size_id) {
        //https://api.digitalocean.com/droplets/[droplet_id]/resize/?size_id=[size_id]&client_id=[your_client_id]&api_key=[your_api_key]
        return getDropletResponse(auth_key, DropletID.toString(), "resize", "size_id=" + size_id);
    }

    public static Action rebuildDroplet(String auth_key, BigInteger DropletID, String image_id) {
        return getDropletResponse(auth_key, DropletID.toString(), "rebuild", "image_id=" + image_id);
    }

    public static Action renameDroplet(String auth_key, BigInteger DropletID, String newName) {
        return getDropletResponse(auth_key, DropletID.toString(), "rename", "name=" + newName);
    }

    public static Action snapshotDroplet(String auth_key, BigInteger DropletID, String snapshotName) {
        return getDropletResponse(auth_key, DropletID.toString(), "snapshot", "name=" + snapshotName);
    }

    public static Action getDropletResponse(String auth_key, String DropletID, String cmd) {
        return getDropletResponse(auth_key, DropletID, cmd, null);
    }


	static Images getImageResponse(String auth_key, BigInteger ImageID, String cmd, String extra) {


		//https://api.digitalocean.com/images/[image_id]/destroy/?client_id=[your_client_id]&api_key=[your_api_key]
		//String url = "https://api.digitalocean.com/images/";
        String url = "https://api.digitalocean.com/v2/images/";
		//url += ImageID + "/";
		//url += cmd + "/?client_id=" + client_id + "&api_key=" + api_key;

		if (extra != null) {
			url += "&" + extra;
		}

		Log.w("myApp", "getImageResponse: " + cmd + ", url " + url);

		InputStream stream = retrieveStream(url, auth_key);

		Gson gson = new Gson();

		Reader reader = new InputStreamReader(stream);

		Images response = gson.fromJson(reader,  Images.class);

		Log.w("myApp", "getImageResponse count " + response.meta.total);

		return(response);
	}

    static Action getDropletResponse(String auth_key, String DropletID, String cmd, String extra) {

        //cmd = reboot, power_cycle
        String url = "https://api.digitalocean.com/v2/droplets/";

        //Create URL
        if (DropletID != null) url += DropletID + "/actions";

        if (extra != null)
            url += "&" + extra;


        //Create JSON
        Type type = new Type();
        type.type = cmd;

        Gson newGson = new Gson();
        String msg = newGson.toJson(type);


        System.out.println( "getDropletResponse: " + cmd +  ", url " + url);

        InputStream stream;
        try {
            stream = retrieveStream(url, auth_key, msg);
        } catch (UnsupportedEncodingException e) {
            return null;
        }

        Gson gson = new Gson();

        Reader reader = new InputStreamReader(stream);

        Action response = gson.fromJson(reader, Action.class);

        System.out.println( "getDropletResponse Action status " + response.status);

        return (response);

    }

	//TODO: Convert all ints to BigInteger

	public static Droplets getDroplets(String auth_key) throws Exception {

		//String url = "https://api.digitalocean.com/droplets/?client_id=" + client_id + "&api_key=" + api_key;
        String url = "https://api.digitalocean.com/v2/droplets/";
		Log.w("myApp", "getDroplets url " + url);

		InputStream stream = retrieveStream(url, auth_key);

		//TODO: Break this out into a separate response checker
		if (statusCode == 401) {
			throw new Exception("Access Denied: Please check your Client ID and API Key");
		}
		if (statusCode == 404) {
			throw new Exception("Not Found: Please check your query");
		}
		if (statusCode != 200) {
			throw new Exception("Error: Status Code = " + statusCode + " from DO");
		}

		Gson gson = new Gson();

		Reader reader = new InputStreamReader(stream);

		Droplets response = gson.fromJson(reader, Droplets.class);

		Log.w("myApp", "GetDroplets count " + response.meta.total);

		return (response);
	}

    //Http post
    private static InputStream retrieveStream(String url, String auth_key, String postMsg) throws UnsupportedEncodingException {

        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost(url);

        StringEntity params = new StringEntity(postMsg);
        request.setEntity(params);

        request.addHeader("Authorization", "Bearer " + auth_key);
        request.addHeader("Content-Type", "application/json");

        try {

            HttpResponse getResponse = client.execute(request);
            statusCode = getResponse.getStatusLine().getStatusCode();

            HttpEntity getResponseEntity = getResponse.getEntity();
            return getResponseEntity.getContent();

        }
        catch (IOException e) {
            request.abort();
            System.out.println("AnalogDesert.retrieveStream: Error for URL " + url + e.getMessage());
        }


        return null;
    }

	private static InputStream retrieveStream(String url, String auth_key) {

		DefaultHttpClient client = new DefaultHttpClient();


		HttpGet getRequest = new HttpGet(url);


        getRequest.addHeader("Authorization","Bearer " + auth_key);

		try {

			HttpResponse getResponse = client.execute(getRequest);
			statusCode = getResponse.getStatusLine().getStatusCode();

			HttpEntity getResponseEntity = getResponse.getEntity();
			return getResponseEntity.getContent();

		} 
		catch (IOException e) {
			getRequest.abort();
			System.out.println("AnalogDesert.retrieveStream: Error for URL " + url + e.getMessage());
		}

		return null;

	}

	public static Sizes getSizes(String auth_key) {
		//https://api.digitalocean.com/sizes/?client_id=[your_client_id]&api_key=[your_api_key]
		//String url = "https://api.digitalocean.com/sizes/?client_id=" + client_id + "&api_key=" + api_key;
        String url = "https://api.digitalocean.com/v2/sizes/";

		Log.w("myApp", "Sizes getSizes URL = " + url);

		InputStream stream = retrieveStream(url, auth_key);

		Log.w("myApp", "StatusCode = " + statusCode);

		Gson gson = new Gson();

		Reader reader = new InputStreamReader(stream);

		Sizes response = gson.fromJson(reader, Sizes.class);

		Log.w("myApp", "DropletStatus response " + response.meta.total);

		return (response);

	}


	public static Images destroyImage(String auth_key, BigInteger ImageID) {
		//https://api.digitalocean.com/images/[image_id]/destroy/?client_id=[your_client_id]&api_key=[your_api_key]			
		return getImageResponse(auth_key, ImageID, "destroy", null);
	}

	public static Images transferImage(String auth_key, BigInteger ImageID, String RegionID) {
		//https://api.digitalocean.com/images/[image_id]/transfer/?client_id=[your_client_id]&api_key=[your_api_key]&region_id=[region_id]
		return getImageResponse(auth_key, ImageID, "transfer", "region_id=" + RegionID);
	}









}
