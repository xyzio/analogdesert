package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkapoor on 9/20/2014.
 */
public class Ssh_keys {

    @SerializedName("Ssh_keys")
    public List<Ssh_key> ssh_keys;

    @SerializedName("meta")
    public Meta meta;
}
