package com.xyzio.analogdesert.libs;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mkapoor on 9/20/2014.
 */
public class Size {

    @SerializedName("slug")
    public String slug;

    @SerializedName("memory")
    public String memory;

    @SerializedName("")
    public String vcpus;

    @SerializedName("disk")
    public String disk;

    @SerializedName("transfer")
    public String transfer;

    @SerializedName("price_monthly")
    public String price_monthly;

    @SerializedName("price_hourly")
    public String price_hourly;

    @SerializedName("regions")
    public List<String> regions;

    public Map<String, String> getKeyValueMap() {

        Map<String, String> keyValueMap = new HashMap<String, String>();

        if (slug != null) keyValueMap.put("size slug", slug);
        if (memory != null) keyValueMap.put("memory size", memory);
        if (vcpus != null) keyValueMap.put("vcpus", vcpus);
        if (disk != null) keyValueMap.put("disk size", disk);
        if (transfer != null) keyValueMap.put("transfer size", transfer);
        if (price_monthly != null) keyValueMap.put("size price_monthly", price_monthly);
        if (price_hourly != null) keyValueMap.put("size price_hourly", price_hourly);
        if (regions != null) keyValueMap.put("size regions", TextUtils.join(", ",regions));

        return keyValueMap;
    }
    /*
        {
      "slug": "512mb",
      "memory": 512,
      "vcpus": 1,
      "disk": 20,
      "transfer": 1,
      "price_monthly": 5.0,
      "price_hourly": 0.00744,
      "regions": [
        "nyc1",
        "sfo1",
        "ams1"
      ]
    },
     */
}
