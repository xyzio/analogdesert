package com.xyzio.analogdesert.libs;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mkapoor on 9/17/2014.
 */
public class Droplet {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("memory")
    public String memory;

    @SerializedName("vcpus")
    public String vcpus;

    @SerializedName("disk")
    public String disk;

    @SerializedName("available")
    public String available;

    @SerializedName("region")
    public Region region;

    @SerializedName("image")
    public Image image;

    @SerializedName("size")
    public Size size;

    @SerializedName("locked")
    public String locked;

    @SerializedName("status")
    public String status;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("networks")
    public Networks networks;

//networks
    /*
    public class V4
{
    public string ip_address { get; set; }
    public string netmask { get; set; }
    public string gateway { get; set; }
    public string type { get; set; }
}

public class V6
{
    public string ip_address { get; set; }
    public int cidr { get; set; }
    public string gateway { get; set; }
    public string type { get; set; }
}

public class Networks
{
    public List<V4> v4 { get; set; }
    public List<V6> v6 { get; set; }
}
     */

    @SerializedName("kernel")
    public Kernel kernel;

    @SerializedName("features")
    public List<String> features;

    @SerializedName("backup_ids")
    public List<String> backup_ids;

    @SerializedName("snapshot_ids")
    public List<String> snapshot_ids;


    /*    {

            "droplet": {
            "id": 20,
                    "name": "test.example.com",
                    "memory": 512,
                    "vcpus": 1,
                    "disk": 20,
                    "Region": {
                "slug": "nyc1",
                        "name": "New York",
                        "sizes": [
                "1gb",
                        "512mb"
                ],
                "available": true,
                        "features": [
                "virtio",
                        "private_networking",
                        "backups",
                        "ipv6"
                ]
            },
            "image": {
                "id": 119192817,
                        "name": "Ubuntu 13.04",
                        "distribution": "ubuntu",
                        "slug": "ubuntu1304",
                        "public": true,
                        "regions": [
                "nyc1"
                ],
                "created_at": "2014-09-05T02:02:05Z"
            },
            "size": {
                "slug": "512mb",
                        "transfer": 1,
                        "price_monthly": 5.0,
                        "price_hourly": 0.00744
            },
            "locked": false,
                    "status": "active",
                    "networks": {
                "v4": [
                {
                    "ip_address": "127.0.0.20",
                        "netmask": "255.255.255.0",
                        "gateway": "127.0.0.21",
                        "type": "public"
                }
                ],
                "v6": [
                {
                    "ip_address": "2001::14",
                        "cidr": 124,
                        "gateway": "2400:6180:0000:00D0:0000:0000:0009:7000",
                        "type": "public"
                }
                ]
            },
            "kernel": {
                "id": 485432986,
                        "name": "DO-recovery-static-fsck",
                        "version": "3.8.0-25-generic"
            },
            "created_at": "2014-09-05T02:02:05Z",
                    "features": [
            "ipv6"
            ],
            "backup_ids": [

            ],
            "snapshot_ids": [

            ]
        }
        }
       */
    public Map<String, String> getKeyValueMap() {

        Map<String, String> keyValueMap = new HashMap<String, String>();

        if (id != null) keyValueMap.put("droplet id", id);
        if (name != null) keyValueMap.put("droplet name", name);
        if (memory != null) keyValueMap.put("droplet memory",memory);
        if (vcpus != null) keyValueMap.put("droplet vcpus", vcpus);
        if (disk != null) keyValueMap.put("droplet disk",disk);
        if (available != null) keyValueMap.put("droplet available",available);
        if (region != null) keyValueMap.putAll(region.getKeyValueMap());
        if (image != null) keyValueMap.putAll(image.getKeyValueMap());
        if (size != null) keyValueMap.putAll(size.getKeyValueMap());
        if (networks != null) keyValueMap.putAll(networks.getKeyValueMap());
        if (kernel != null) keyValueMap.putAll(kernel.getKeyValueMap());
        if (features != null) keyValueMap.put("droplet features", TextUtils.join(", ", features));
        if (backup_ids != null) keyValueMap.put("droplet backup ids", TextUtils.join(", ", backup_ids));
        if (snapshot_ids != null) keyValueMap.put("droplet snapshot ids", TextUtils.join(", ", snapshot_ids));

        return keyValueMap;
    }

}
