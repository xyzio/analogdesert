package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mkapoor on 9/21/2014.
 */
public class V4 {
    @SerializedName("ip_address")
    public String ip_address;

    @SerializedName("netmask")
    public String netmask;

    @SerializedName("gateway")
    public String gateway;

    @SerializedName("type")
    public String type;

    public Map<String, String> getKeyValueMap() {

        Map<String, String> keyValueMap = new HashMap<String, String>();

        keyValueMap.put("network ip_address",ip_address);
        keyValueMap.put("network netmask", netmask);
        keyValueMap.put("network gateway", gateway);
        keyValueMap.put("network type", type);

        return keyValueMap;

    }
}
