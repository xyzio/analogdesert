package com.xyzio.analogdesert.libs;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mkapoor on 9/20/2014.
 */
public class Region {

    @SerializedName("slug")
    public String slug;

    @SerializedName("name")
    public String name;

    @SerializedName("sizes")
    public List<String> sizes;

    @SerializedName("available")
    public String available;

    @SerializedName("features")
    public List<String> features;

    public Map<String, String> getKeyValueMap() {

        Map<String, String> keyValueMap = new HashMap<String, String>();

        keyValueMap.put("region slug", slug);
        keyValueMap.put("region name", name);
        keyValueMap.put("region sizes", TextUtils.join(", ", sizes) );
        keyValueMap.put("region available", available);
        keyValueMap.put("region features", TextUtils.join(", ", features));

        return keyValueMap;
    }

    /*
    {
      "slug": "nyc1",
      "name": "New York",
      "sizes": [

      ],
      "available": false,
      "features": [
        "virtio",
        "private_networking",
        "backups",
        "ipv6"
      ]
    },
    {
      "slug": "sfo1",
      "name": "San Francisco",
      "sizes": [
        "1gb",
        "512mb"
      ],
      "available": true,
      "features": [
        "virtio",
        "backups"
      ]
    },
     */
}
