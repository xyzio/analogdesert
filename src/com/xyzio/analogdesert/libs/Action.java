package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mkapoor on 9/16/2014.
 */
public class Action {

        @SerializedName("id")
        public String id;

        @SerializedName("status")
        public String status;

        @SerializedName("type")
        public String type;

        @SerializedName("started_at")
        public String started_at;

        @SerializedName("completed_at")
        public String completed_at;

        @SerializedName("resource_id")
        public String resource_id;

        @SerializedName("resource_type")
        public String resource_type;

        @SerializedName("Region")
        public String region;

}
