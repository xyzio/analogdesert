package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkapoor on 9/17/2014.
 */

public class Domain_records {
    @SerializedName("Domain_records")
    public List<Domain_record> domain_records;

    @SerializedName("Meta")
    public Meta meta;

}
