package com.xyzio.analogdesert.libs;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mkapoor on 9/28/2014.
 */
public class Type {
    @SerializedName("type")
    public String type;
}
