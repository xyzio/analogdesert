package com.xyzio.analogdesert;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ListInputDialogFragment extends DialogFragment {

	ArrayList<Integer> mSelectedItems;
	ArrayList<String> arrayList = new ArrayList<String>();
	
	//CALBACKS
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String result);
        public void onDialogNegativeClick(DialogFragment dialog);
    }
    
    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;
    
    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
    //-----------------------

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mSelectedItems = new ArrayList<Integer>();  // Where we track the selected items
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        //String[] array = getResources().getStringArray(R.array.Images);
        
        arrayList = (ArrayList<String>) getArguments().getStringArrayList("list");
        String[] array = new String[arrayList.size()];
        arrayList.toArray(array);

        // Set the dialog title	    
        builder.setTitle(getArguments().getString("sTitle", ""))
        
     
        // Specify the list array, the items to be selected by default (null for none),
        // and the listener through which to receive callbacks when items are selected
               .setSingleChoiceItems(array, 0,
                          new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int which) {
                           mSelectedItems.add(which);
                   }
               })
        // Set the action buttons
               .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int id) {
                       // User clicked OK, so save the mSelectedItems results somewhere
                       // or return them to the component that opened the dialog
                       //...
                	   String result = "";
                	   try {
                		   result = mSelectedItems.get(0).toString();
                	   }
                	   catch (Exception ex) {
                		   result = "0"; //Return 0 as in the zeroth index.
                	   }
                	   
                	   mListener.onDialogPositiveClick(ListInputDialogFragment.this, result);
                   }
               })
               .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialog, int id) {
                	   mListener.onDialogNegativeClick(ListInputDialogFragment.this);
                       //...
                   }
               });

        return builder.create();
    }
}