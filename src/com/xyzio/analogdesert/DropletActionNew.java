package com.xyzio.analogdesert;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.xyzio.analogdesert.libs.AnalogDesertInterface;
import com.xyzio.analogdesert.libs.Droplet;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


//TODO: Convert this to listview in the future!!

public class DropletActionNew extends FragmentActivity implements ListInputDialogFragment.NoticeDialogListener {

	List<Droplet> droplets = new ArrayList<Droplet>();
	//List<Size> sizes = new ArrayList<Size>();

	BigInteger[] dropletIDs;
	String action = "";
	String metadata = "";
	String name = "";

	String imageID = "";
	String sizeID = "";
	String regionID = "";

	ArrayList<String> imageIdToIndexMap = new ArrayList<String>();
	ArrayList<String> sizeIdtoIndexMap = new ArrayList<String>();	
	ArrayList<String> regionIdToIndexMap = new ArrayList<String>();

	ArrayList<String> imageList = new ArrayList<String>();
	ArrayList<String> sizeList = new ArrayList<String>();
	ArrayList<String> regionList = new ArrayList<String>();


	String currentAction = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_droplet_action_new);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			//Stuff to get with adapter
			action  = extras.getString("action", "");
			Log.w("myApp", "Getting action: " + action);
			setTitle(extras.getString("title", action));
		}

		onResume();		
	}

	@Override
	public void onResume() {
		super.onResume();
	}


	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.buttonSelectIP:
			currentAction = "get_image";
			new DownloadImages().execute();
			break;
		case R.id.buttonCreate:
			currentAction = "get_size";
			new DownloadSizes().execute();
			break;
		case R.id.button3:
			currentAction = "get_region";
			new DownloadRegions().execute();
			break;
		case R.id.button4:
			currentAction = "create";
			Create();
			break;
		}
	}

	public void Create() {

		EditText nameEditText = (EditText) findViewById(R.id.editText1);
		name = nameEditText.getText().toString();

		if (name == null || name.isEmpty()) {
			Toast.makeText(getBaseContext(), "Error: Droplet Name is null or empty", Toast.LENGTH_LONG).show();
			return;
		}
		if (imageID == null || imageID.isEmpty()) {
			Toast.makeText(getBaseContext(), "Error: Invalid Image", Toast.LENGTH_LONG).show();
			return;
		}
		if (sizeID == null || sizeID.isEmpty()) {
			Toast.makeText(getBaseContext(), "Error: Invalid Size", Toast.LENGTH_LONG).show();
			return;
		}
		if (regionID == null || regionID.isEmpty()) {
			Toast.makeText(getBaseContext(), "Error: Invalid Region", Toast.LENGTH_LONG).show();
			return;
		}
		
		new CreateDroplet().execute();
		
		//Toast.makeText(getBaseContext(), "Create" + "Image: " + imageID + "Size: " + sizeID + "Region: " + regionID, Toast.LENGTH_LONG).show();
		//This is where we create
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String result) {
		// User touched the dialog's positive button
		Integer nResult = Integer.parseInt(result);
		if (currentAction.toLowerCase().contains(("get_image"))) {
			imageID = imageIdToIndexMap.get(nResult);

			Button button =	(Button)findViewById(R.id.buttonSelectIP);
			button.setText(imageList.get(nResult));
			dialog.dismiss();
		}

		if (currentAction.toLowerCase().contains("get_size")) {
			sizeID = sizeIdtoIndexMap.get(nResult);

			Button button = (Button)findViewById(R.id.buttonCreate);
			button.setText(sizeList.get(nResult));
			dialog.dismiss();
		}

		if (currentAction.toLowerCase().contains("get_region")) {
			regionID = regionIdToIndexMap.get(nResult);

			Button button = (Button)findViewById(R.id.button3);
			button.setText(regionList.get(nResult));
			dialog.dismiss();
		}
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// User touched the dialog's negative button
		//Toast.makeText(getBaseContext(), "Cancelled", Toast.LENGTH_LONG).show();

		//finish();
	}

	//---------------------
	class DownloadImages extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Images> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletActionNew.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletActionNew.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Downloading Images ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Images doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletActionNew.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Images images = new com.xyzio.analogdesert.libs.Images();

			try {
				images = AnalogDesertInterface.getImages(auth_key, null);

			} catch (Exception e) {
				exceptionMessage = e.toString();
			}

			return (images); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Images weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();


            //TODO: Fix error logging
			/*if ((weather_data == null) || (weather_data.status.toLowerCase().contains("error") == true)) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(exceptionMessage + " " + weather_data.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else*/ if (weather_data.images.size() > 0) {
				imageList.clear();
				imageIdToIndexMap.clear();
				imageID = "";

				Toast.makeText(getBaseContext(), "Found " + weather_data.images.size() + " images", Toast.LENGTH_LONG).show();
				for (int i = 0; i < weather_data.images.size(); i += 1) {
					imageList.add(weather_data.images.get(i).name);
					imageIdToIndexMap.add(weather_data.images.get(i).id + "");		
				}

				//Rebuild. Need to get image ID for base
				DialogFragment newFragment = new ListInputDialogFragment();
				Bundle args = new Bundle();
				args.putString("sTitle", "Select Image");
				args.putStringArrayList("list", imageList);

				newFragment.setArguments(args);

				newFragment.show(getSupportFragmentManager(), "getImage");
			}
		}
	}

	//---------------------
	//---------------------
	class CreateDroplet extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Droplets> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletActionNew.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletActionNew.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Creating Droplet ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Droplets doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletActionNew.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Droplets droplets = new com.xyzio.analogdesert.libs.Droplets();

			try {
				//droplets = AnalogDesertInterface.createDroplet(auth_key, name, sizeID, imageID, regionID);

			} catch (Exception e) {
				exceptionMessage = e.toString();
			}

			return (droplets); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Droplets weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();

            //TODO: Reenable error logging
			/*if ((weather_data == null) || (weather_data.status.toLowerCase().contains("error") == true)) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(exceptionMessage + " " + weather_data.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else*/ if (weather_data.droplets != null) {
				errorDialog.setTitle("Success");

                 //TODO: Fix create droplet code below
				String message = "Droplet Created\n";
				message += "Droplet Name: " + weather_data.droplets.get(0).name + "\n";
				message += "Droplet ID: " + weather_data.droplets.get(0).id + "\n";
				//message += "Event ID: " + weather_data.droplets.get(0).event_id + "\n";

				errorDialog.setMessage(message);
				
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
		}
	}
	//---------------------
	//---------------------
	class DownloadRegions extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Regions> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletActionNew.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletActionNew.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Downloading Regions ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Regions doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletActionNew.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Regions regions = new com.xyzio.analogdesert.libs.Regions();
			try {
				regions = AnalogDesertInterface.getRegions(auth_key);
			}
			catch (Exception ex){
				exceptionMessage = ex.getMessage();
			}
			return (regions); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Regions weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();


			if (weather_data == null) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(exceptionMessage);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else if (weather_data.regions.size() > 0) {
				regionList.clear();
				regionIdToIndexMap.clear();
				regionID = "";

				Toast.makeText(getBaseContext(), "Found " + weather_data.regions.size() + " regions", Toast.LENGTH_LONG).show();
				for (int i = 0; i < weather_data.regions.size(); i += 1) {
					regionList.add(weather_data.regions.get(i).name);
					regionIdToIndexMap.add(weather_data.regions.get(i).slug + "");
				}

				//Rebuild. Need to get image ID for base
				DialogFragment newFragment = new ListInputDialogFragment();
				Bundle args = new Bundle();
				args.putString("sTitle", "Select Region");
				args.putStringArrayList("list", regionList);

				newFragment.setArguments(args);

				newFragment.show(getSupportFragmentManager(), "getRegion");
			}
		}
	}


	//---------------------
	//http://developer.android.com/reference/android/os/AsyncTask.html
	class DownloadDroplets extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Droplets> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletActionNew.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletActionNew.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Downloading Droplets ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Droplets doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletActionNew.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Droplets droplets = new com.xyzio.analogdesert.libs.Droplets();

			try {
				droplets = AnalogDesertInterface.getDroplets(auth_key);
			} catch (Exception e) {
				exceptionMessage = e.toString();
			}

			//TODO: Read response = ERROR too

			return (droplets); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Droplets weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();

            //TODO: FIX error logging
			/*if ((weather_data == null) || (weather_data.status.toLowerCase().contains("error") == true)) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(exceptionMessage + " " + weather_data.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else*/ if (weather_data.droplets.size() > 0) {
				droplets.clear();
				droplets.addAll(weather_data.droplets);
			}

		}
	}

	class DownloadSizes extends AsyncTask<Void, Void, com.xyzio.analogdesert.libs.Sizes> {

		private final AlertDialog errorDialog = new AlertDialog.Builder(DropletActionNew.this).create();
		private final ProgressDialog dialog = new ProgressDialog(DropletActionNew.this); //See more at: http://www.survivingwithandroid.com/2013/01/android-async-listview-jee-and-restful.html#sthash.darO5D4G.dpuf


		String exceptionMessage = "";

		protected void onPreExecute() {       
			super.onPreExecute();

			dialog.setMessage("Downloading Sizes ...");
			dialog.show();
		}

		protected com.xyzio.analogdesert.libs.Sizes doInBackground(Void... arg0) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(DropletActionNew.this);

			String auth_key = sharedPref.getString("auth_key", "");
			//String client_id = sharedPref.getString("pref_client_id", "");

			com.xyzio.analogdesert.libs.Sizes sizes = new com.xyzio.analogdesert.libs.Sizes();

			try {
				sizes = AnalogDesertInterface.getSizes(auth_key);
			} catch (Exception ex) {
				exceptionMessage = ex.getMessage();

			}
			return (sizes); 
		}

		protected void onPostExecute(com.xyzio.analogdesert.libs.Sizes weather_data) {
			super.onPostExecute(weather_data);	

			dialog.dismiss();

			/*if ((weather_data == null) || (weather_data.status.toLowerCase().contains("error") == true)) {
				errorDialog.setTitle("Error");
				errorDialog.setMessage(exceptionMessage + " " + weather_data.error_message);
				errorDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});
				errorDialog.show();
			}
			else*/ if (weather_data.sizes.size() > 0) {
				sizeList.clear();
				sizeIdtoIndexMap.clear();
				sizeID = "";

                //TODO: fix get size menu
				/*for (int i = 0; i < weather_data.sizes.size(); i += 1) {
					sizeList.add(weather_data.sizes.get(i).name);
					sizeIdtoIndexMap.add(weather_data.sizes.get(i).id + "");		
				}*/

				//Rebuild. Need to get image ID for base
				DialogFragment newFragment = new ListInputDialogFragment();
				Bundle args = new Bundle();
				args.putString("sTitle", "Select Size");
				args.putStringArrayList("list", sizeList);

				newFragment.setArguments(args);

				newFragment.show(getSupportFragmentManager(), "getSize");
			}
		}
	}
}

